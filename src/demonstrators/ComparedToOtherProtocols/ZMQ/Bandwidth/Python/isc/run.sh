#!/bin/bash
# ZMQ isc


echo "#######################"

BASEPORT="888"
HUB="0"
MASTER="1"

BASESEND1="tcp://"
BASESEND2=":8881"
RECEIVEURL="tcp://*:8881"


Pi0="192.168.0.200"
Pi1="192.168.0.201"
Pi2="192.168.0.202"
Pi3="192.168.0.203"

# copy MQTT.py the raspberries 
scp ZMQbisc.py pi@${Pi0}:~/ZMQbisc.py
scp ZMQbisc.py pi@${Pi1}:~/ZMQbisc.py
scp	ZMQbisc.py pi@${Pi3}:~/ZMQbisc.py

ssh pi@${Pi0} 'mkdir /tmp/feeds'
ssh pi@${Pi1} 'mkdir /tmp/feeds'
ssh pi@${Pi3} 'mkdir /tmp/feeds'

SOCKETLOCATION="ipc:///tmp/feeds"

mkdir /tmp/feeds

trap ctrl_c INT

ctrl_c() {
		echo "knock knock, cleaning lady"
       	sh cleanup.sh
}

ssh pi@${Pi0} python3 ~/ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi1}${BASESEND2}  0 &
sleep 1
ssh pi@${Pi1} python3 ~/ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi3}${BASESEND2}  0 &
sleep 1
ssh pi@${Pi3} python3 ~/ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi2}${BASESEND2}  0 &

python3 ZMQbisc.py ${RECEIVEURL} ${BASESEND1}${Pi0}${BASESEND2} 1


sh cleanup.sh

echo "#######################"

