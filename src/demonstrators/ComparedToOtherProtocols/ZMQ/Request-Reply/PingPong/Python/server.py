import time
import zmq

context = zmq.Context() 
socket = context.socket(zmq.REP) # set this as a replying node/hub/
socket.bind("tcp://*:5555") # receive and reply on port 5555

while True:
    message = socket.recv()
    print("received request %s" % message)
    
    time.sleep(1)
    
    socket.send(b"world") # reply to request
    