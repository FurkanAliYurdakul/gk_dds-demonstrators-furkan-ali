#ifndef DDS_CREATE_ENTITIES_H
#define DDS_CREATE_ENTITIES_H
 
dds_entity_t create_topic(int MBC_number, dds_entity_t participant);
dds_entity_t create_reader(dds_entity_t participant, dds_entity_t topic);
dds_entity_t create_writer(dds_entity_t participant , dds_entity_t topic);
 
#endif