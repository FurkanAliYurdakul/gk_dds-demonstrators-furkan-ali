#! /usr/bin/env python3

import sys, time, os
import random
import cdds as dds
from MatrixTopic import MatrixTopic
from _aux import process_from, send_data 

all_possible_nodes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
sending_node = int(sys.argv[1])
topic_prefix = "mb{}"

global speed, old_traffic, receiving_topic_readers, nr_of_listeners, preferred_topic_readers, sending_topic
sending_topic = topic_prefix.format(sending_node)

LOOP_SLEEP = 6
MAX_SPEED = 120
LOW_SPEED = 50
speed = MAX_SPEED
pid=os.getpid()
old_traffic = 0
nr_of_listeners = 0

# adapt the speed based on the traffic from the receivers
def adapt_speed(traffic):
    global old_traffic
    global speed
    global sending_topic
    if old_traffic < traffic:
        if speed > LOW_SPEED:
            speed -= 10
    if old_traffic > traffic:
        if speed < MAX_SPEED:
            speed += 10
    print("[%i] [%s] Adapting speed to: %i" % (pid, sending_topic, speed))
    old_traffic = traffic

# generate traffic and send it to all receivers
def generate_traffic():
    global sender
    global sending_topic
    own_traffic = random.randrange(10, 50, 1)
    print("[%i] [%s] Sending generated traffic: %i" % (pid, sending_topic, own_traffic))
    send_data(sender, own_traffic)

# remove a matched reader
def remove_listener(listener):
    global nr_of_listeners
    global receiving_topic_readers
    receiving_topic_readers.remove(listener)
    nr_of_listeners -= 1
    print("removed listener, number of listeners: %i" % (nr_of_listeners))

# process the data of a matching subscription for a reader
# only the data of the first 2 matched readers will be processed
def on_subscription_matched(listener):
    global nr_of_listeners
    global receiving_topic_readers
    global preferred_topic_readers
    if nr_of_listeners < 2:
        if listener in receiving_topic_readers:
            process_from(listener, adapt_speed, remove_listener)
        else:
            receiving_topic_readers.append(listener)
            nr_of_listeners += 1
            process_from(listener, adapt_speed, remove_listener)
        print("subscription matched, number of listeners: %i" % (nr_of_listeners))
    else:
        if listener in preferred_topic_readers and listener not in receiving_topic_readers:
            for reader in receiving_topic_readers:
                if reader not in preferred_topic_readers:
                    remove_listener(reader)
                    receiving_topic_readers.append(listener)
                    nr_of_listeners += 1
                    print("subscription matched preferred reader, number of listeners: %i" % (nr_of_listeners))
                    break;

        if listener in receiving_topic_readers:
           process_from(listener, adapt_speed, remove_listener)

# create readers for remainging topics
def create_list_of_receiving_topic_readers(dp, sending_node):
    global preferred_topic_readers
    for receiving_node in all_possible_nodes:
        if receiving_node > sending_node:
            t_in = MatrixTopic(dp, topic_prefix.format(receiving_node))
            reader = dds.FlexyReader(dp, t_in, on_subscription_matched, [dds.Reliable(), dds.KeepLastHistory(10)])
            if receiving_node <= sending_node + 2:
                preferred_topic_readers.append(reader)

# main loop
if __name__ == '__main__':
    global sender

    receiving_topic_readers = []
    preferred_topic_readers = []
    rt = dds.Runtime()
    dp = dds.Participant(0)
    create_list_of_receiving_topic_readers(dp, sending_node)
    t_out = MatrixTopic(dp, sending_topic)
    sender = dds.FlexyWriter(dp, t_out, [dds.Reliable(), dds.KeepLastHistory(10)])

    print("starting")

    while True:
        time.sleep(LOOP_SLEEP)
        generate_traffic()
