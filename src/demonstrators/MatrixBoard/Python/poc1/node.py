#!/usr/bin/env python3

import cdds
from cdds import Topic
import time
import sys
import random
from pynput import keyboard

'''
This class defines the topic to be published.
In our case all nodes/poles publish to this topic

The crowdness is the indicator of the level of traffic jam
I did not use 'the number of cars passing by' because it means the smoothness of the traffic.
Because using crowdness seems more direct.

'''
class TrafficInfo(Topic):
    def __init__(self, marker_id, location_number, speed_limit_display, crowdness):
        super(Topic,self).__init__()
        self.marker_id = marker_id
        self.location_number = location_number
        self.speed_limit_display = speed_limit_display
        self.crowdness = crowdness # the level of traffic jam
    
    def gen_key(self):
        return self.marker_id

    def sensing(self):
        self.crowdness = random.randint(0,30)

    def __str__(self):
        return 'TrafficInfo({0},{1},{2},{3})'.format(self.marker_id, self.location_number, self.speed_limit_display, self.crowdness)



'''
This class defines the pole.
Every pole act as a participant of a domain.
'''
# https://en.wikipedia.org/wiki/Highway_location_marker
# Dutch hectometre => Afstandspaal
# https://nl.wikipedia.org/wiki/Afstandspaal
class HighwayLocationMarker():

    def __init__(self, did, marker_id, location_number):
        self.marker_id = marker_id
        self.location_number = location_number
        self.speed_limit_display = 130
        self.sensed_traffic = 0
        self.did = did
        self.topic_name = 'Traffics'
        self.next_nearest_neighbor = None
        self.dp_list = set() # a list of the location number of the dp's in the domain. Use set here to make sure every element is unique.
        self.dp_list.add(self.location_number)
        self.traffic_info = TrafficInfo(self.marker_id, self.location_number, self.speed_limit_display, self.sensed_traffic)

    def update_next_nearest_neighbor(self):
        result = 0
        smallest_difference = 2000000
        for pole in self.dp_list:
            if pole - self.location_number > 0 and pole - self.location_number < smallest_difference:
                smallest_difference = pole-self.location_number
                result = pole
        self.next_nearest_neighbor = result
        self.dp_list.add(self.next_nearest_neighbor)

    def change_on_key_press(self, key):
        # @todo, Do something on key press, e.g. make a direct traffic jam or set speed limit to value x
        if key in space_key:
            print("Space Key pressed!")

    def change_display(self, nnn):
        # @todo change the display according to the input of the neighbour
        nnn_speed_limit = nnn.speed_limit_display
        print("Neighbour speed limit: {0} km/h; My speed limit {1}".format(nnn_speed_limit, self.speed_limit_display))

    def data_available(self, r):
        samples = r.take(cdds.all_samples())
        for s in samples:
            if s[1].valid_data:
                # check the new comming message, update the domain participant list
                if s[0].location_number not in self.dp_list:
                    # new participant detected in the domain
                    self.dp_list.add(s[0].location_number)
                if s[0].location_number == self.next_nearest_neighbor:
                    # process the message from the neigbour
                    self.change_display(s[0])
        self.update_next_nearest_neighbor()
        # print(self.dp_list)
        # print("The next nearesrt neighbour is {}".format(self.next_nearest_neighbor))
          

    def liveliness_changed(self, r, e):
        # this happens when a new participant joins or quits
        print("Someone joined or quited!")
        self.dp_list.clear()
        self.dp_list.add(self.location_number)
        pass

    def initialize(self):
        # Always create a runtime to initialise DDS
        self.rt = cdds.Runtime()
        self.dp = cdds.Participant(self.did)
        self.t = cdds.FlexyTopic(self.dp, self.topic_name)
        self.w = cdds.FlexyWriter(self.dp, self.t, [cdds.Reliable(), cdds.KeepLastHistory(10)])
        self.r = cdds.FlexyReader(self.dp, self.t, self.data_available ,[cdds.Reliable(), cdds.KeepLastHistory(10)])
        self.r.on_liveliness_changed(self.liveliness_changed)

    def run(self):
        while True:
            self.w.write(self.traffic_info)
            print('Wrote: {0}'.format(self.traffic_info))
            self.traffic_info.sensing()
            time.sleep(2)

            


if __name__ == '__main__':
    space_key = [keyboard.Key.space]
    l_nr = float(sys.argv[1]) if len(sys.argv) > 1 else 17.1
    node = HighwayLocationMarker(did=0, marker_id = 0, location_number =l_nr)
    lis = keyboard.Listener(on_press=node.change_on_key_press) 
    lis.start()
    node.initialize()
    node.run()