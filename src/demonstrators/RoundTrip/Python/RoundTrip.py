#! /usr/bin/env python3

import sys,time
import cdds as dds
from datetime import datetime
from _aux import EVERY, process_from, send_data, demo_out_in, M_OUT, M_IN

SendingTopic   = M_OUT
ReceivingTopic = M_IN

MAX = int(sys.argv[1]) if len(sys.argv) >1 else 1000
START_SLEEP = 6
LOOP_SLEEP  = 6

def report(c, t0):
    t1 = datetime.now()
    dt = (t1-t0).total_seconds()
    print("Processed %i messages, in %2.1f seconds: %i msg/sec [%3.3f ms/msg]" % (
        c, dt,
        c/dt,
        0.0 if c==0 else dt*1000/c))

def demo_samples(msg,c):
    global count
    if (count % EVERY) < c:
        print("RoundTrip: sample[%i]: %s" % (count, msg))
    count+=c

def plus_1(v):
    global sender
    send_data(sender, v+1)

def on_receive_data(listner):
    global STOP
    msg,c = process_from(listner, plus_1)
    demo_samples(msg,c);
    if msg.val >= MAX:
        STOP=True


if __name__ == "__main__":
    global sender, val
    global STOP
    global count
    val = None
    STOP=False
    count=0

    rt = dds.Runtime()
    dp = dds.Participant(0)
    t_out = dds.FlexyTopic(dp,  SendingTopic) # set sending topic
    t_in  = dds.FlexyTopic(dp,  ReceivingTopic) # set receiving topic

    sender    = dds.FlexyWriter(dp, t_out, [dds.Reliable(), dds.KeepLastHistory(10)])
    receiver  = dds.FlexyReader(dp, t_in, on_receive_data, [dds.Reliable(), dds.KeepLastHistory(10)])

    demo_out_in(SendingTopic, ReceivingTopic)

    print("Wait a bit ...")
    time.sleep(START_SLEEP)


    print("START: RoundTrip 1->4, 2->4, ...->%s (assuming Loop==4)" % MAX)
    val = 1
    t0 = datetime.now()
    send_data(sender, val)

    while not STOP:
        time.sleep(LOOP_SLEEP)
        report(count, t0)
