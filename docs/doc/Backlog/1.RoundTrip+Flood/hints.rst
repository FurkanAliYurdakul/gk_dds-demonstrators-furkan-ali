.. (C) ALbert Mietus, Sogeti, 2019

Hints
=====


Todo
----

.. todo:: SetUp the M&B comparison between DDS and other protocols in this environment

   .. tip:: Is this part of “DDS-Demonstrators”, or another ``HighTech-NL TechPush`` project?


More links
----------
* About the setup with some raspberry’s see :ref:`setup_pi_Talk_like_pi`

* Some roundtrip/flood see :ref:`RoundTrip_Talk_like_pi` 
