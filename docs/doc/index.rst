.. (C) ALbert Mietus, Sogeti, 2019

***************************
TechPush: DDS-Demonstrators
***************************

.. sidebar:: Work just started

   * All sources, including for documentation can be found in the official *main repository*:
     https://bitbucket.org/HighTech-nl/dds-demonstrators

   * All development is done in *Feature forks*, using the `Forking Workflow
     <https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow>`_

   * **ONLY** the *main repository* is used to generate the documentation:
     https://DDS-Demonstrators.readthedocs.io/

DDS [#DDS]_ (Data Distribution Service) is a real-time, embedded *"data-centric"* **pub-sub** communication framework. Which is
becoming popular in the (professional, industrial) IoT-world. It is also very *HighTech*; one of the reasons why we
(Sogeti HighTech BL) are interested.

Sogeti-HighTech has bundled several innovation-projects into this **DDS-Demonstrators** program.
|BR|
Each project will result in a simple *Demonstrator*, showing the power of DDS; and *how* to *use* DDS; including
(demonstration) code, test, and lessons-learned [#PR1]_.

Most software is made in C, C++ or Python and will be run an embedded Linux system. Often we use a Raspberry Pi, which is
affordable; and a good reference for a modern embedded system.

* Python is mainly used as a Proof-of-Concept language; when the *execution* speed is less relevant than the
  development-velocity. Python is also  used for scripting (for tests and measurements).
* When speed- or latency-measurements are needed, C/C++ is typically used; possibly after a Python PoC.
* Python is also often used for tool-like demonstrators (such as the :ref:`XScope`)

==============
Measure & Blog
==============

The DDS-Demonstrators use the "Measure & Blog" approach: every project and every iteration starts with an (R&D)
question, and ends with a publication on the answer.
|BR|
Typical, the project included some "measurements", and the publication is a simple "blog".

The intermediate results, like code, automatic test- and measure-scripts, and other documentation are also *published*:
should be stored in this version control system. In such a way that others can repeat the experiment easily and learn quickly.

Often, each "Measure & Blog" will lead to new questions. They should also be documented and can lead to new iterations.

More on this, later in a generic blog ...

Deliverables
============

Every iteration [#iteration]_ sprint gives the following deliveries:

#) The *production code* (read: a DDS-Demonstrator, in functional, partial deliveries).
#) The design, where relevant: Use plantUML diagrams, text, etc.
#) The test- and measurement-code.
#) User manuals (SUM: Software User Manuals) - of the product (how to install, run, use ...).
#) Project / Sprint documentation (including estimates, comments).
#) The "Blog": from research question by the measurements/results towards the conclusions/advice; typically short.
#) All kind of â€œconvenient notesâ€; in the :ref:`TeamPages`
#) "Free" documentation improvements on existing (used) documentation *--without adding cost*.
#) All other short, cheap improvements to record knowledge.

.. rubric:: Footnotes

.. [#DDS]  See: https://en.wikipedia.org/wiki/Data_Distribution_Service
.. [#PR1]  To share this knowledge many *DDS-Demonstrators* are treated as open-source. Then it becomes easy to share it
           with our (over 100) HighTech Software Developers, other Sogeti-professionals, and to show it to clients.
.. [#iteration] The term iteration is used to generalize a (scrum) **sprint**. The same deliverables are expected at the
                end of a *super-sprint* (in safe: "PI"), an internship, an innovation-assignment and the end of a
                project.

             
Main Table of Content
=====================

.. toctree::
   :titlesonly:
   :glob:

   Introduction/index
   Installation/index
   Backlog/index
   Demonstrators/index
   Teams/index
   todoList


