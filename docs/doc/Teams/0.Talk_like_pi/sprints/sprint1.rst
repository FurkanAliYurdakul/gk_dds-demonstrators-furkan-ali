
sprint1
=======


sprint1 results
***************

   
   In this sprint the amount it takes for a message to make a round trip has been measured. To understand what the times mean in the tables, see the theory about roundtrip in  :doc:`../RoundTrip`
   The measurements using the round trip programs have given the following results:
   
   +-----------------------+-----------------------+
   | IPC measurements                              |
   +-----------------------+-----------------------+
   | Language:               python3               |
   +-----------------------+-----------------------+
   | protocol              |  ms/msg               |
   +-----------------------+-----------------------+
   | MQTT                  | 4.694029412           |
   +-----------------------+-----------------------+
   | ZMQ                   | 0.540805556           |
   +-----------------------+-----------------------+
   | DDS                   | under construction    |
   +-----------------------+-----------------------+
   
   ms/msg represents the amount of time it takes for the message to make a round trip. 
   
   +-----------------------+-----------------------+
   | ISC measurements                              |
   +-----------------------+-----------------------+
   | Language:               python3               |
   +-----------------------+-----------------------+
   | protocol              |  ms/msg               |
   +-----------------------+-----------------------+
   | MQTT                  | 6.052571429           |
   +-----------------------+-----------------------+
   | ZMQ                   | 1.617088235           |
   +-----------------------+-----------------------+
   | DDS                   | under construction    |
   +-----------------------+-----------------------+